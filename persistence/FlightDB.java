/*
 * Created by Martin Endrst as part of MI-MDW course at CTU FIT
 */
package persistence;

import java.util.ArrayList;
import java.util.List;
import model.BaseEntity;
import model.Flight;

/**
 * @author Martin Endrst
 */
public class FlightDB {
    private static final List<Flight> database = new ArrayList<Flight>();
    private static int lastId = 0;
    
    public static final void persist(Flight flight){
        flight.id = ++lastId;
        database.add(flight);
    }
    
    public static final Flight find(int id){
        for (Flight flight : database){
            if (flight.id == id) return flight;
        }
        return null;
    }
    
    public static final List<Flight> findAll(){
        return database;
    }
    
    public static final void delete(int id){
        Flight flight = find(id);
        if (flight != null){
            database.remove(flight);
        }
    }
    
    public static final List<Flight> findByDestination(String destination){
        List<Flight> retval = new ArrayList<Flight>();
        for (Flight flight : database){
            if (flight.destination.equals(destination)){
                retval.add(flight);
            }
        }
        return retval;
    }
    
}
