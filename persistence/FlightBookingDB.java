/*
 * Created by Martin Endrst as part of MI-MDW course at CTU FIT
 */
package persistence;

import java.util.ArrayList;
import java.util.List;
import model.FlightBooking;

/**
 * @author Martin Endrst
 */
public class FlightBookingDB {
    private static final List<FlightBooking> database = new ArrayList<FlightBooking>();
    private static int lastId = 0;
    
    public static void persist(FlightBooking booking){
        booking.id = ++lastId;
        database.add(booking);
    }
    
    public static FlightBooking find(int id){
        for (FlightBooking booking : database){
            if (booking.id == id) return booking;
        }
        return null;
    }
    
    public static List<FlightBooking> findAll(){
        return database;
    }
    
    public static void delete(int id){
        FlightBooking booking = find(id);
        if (booking != null){
            database.remove(booking);
        }
    }
    
    public static void update(FlightBooking booking){
        FlightBooking persistedBooking = find(booking.id);
        if (persistedBooking != null){
            database.remove(persistedBooking);
        }
        database.add(booking);
    }
}
