/*
 * Created by Martin Endrst as part of MI-MDW course at CTU FIT
 */
package flight_service;

import fault.CapacityInsufficient;
import fault.FlightBookingNotFound;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import model.FlightBooking;
import fault.FlightNotFound;
import java.util.Date;
import javax.xml.bind.annotation.XmlElement;
import model.Flight;
import persistence.FlightBookingDB;
import persistence.FlightDB;

/**
 *
 * @author Martin Endrst
 */
@WebService(serviceName = "FlightService")
public class FlightService {
    
    @WebMethod(operationName = "createFlight")
    @WebResult(name="flight")
    public Flight createFlight(@WebParam(name="destination") @XmlElement(required = true) String destination,
                               @WebParam(name="departureAirport") @XmlElement(required = true) String departureAirport,
                               @WebParam(name="departureDate") @XmlElement(required = true) Date departureDate,
                               @WebParam(name="arrivalAirport") @XmlElement(required = true) String arrivalAirport,
                               @WebParam(name="arrivalDate") @XmlElement(required = true) Date arrivalDate,
                               @WebParam(name="capacity") @XmlElement(required = true) int capacity){
        Flight flight = new Flight(destination, departureAirport, departureDate, arrivalAirport, arrivalDate, capacity);
        FlightDB.persist(flight);
        return flight;
    }
    
    @WebMethod(operationName = "listFlights")
    @WebResult(name="flight")
    public List<Flight> listFlights(){
        return FlightDB.findAll();
    }

    @WebMethod(operationName = "listFlightBookings")
    @WebResult(name="booking")
    public List<FlightBooking> listFlightBookings() {
        return FlightBookingDB.findAll();
    }

    @WebMethod(operationName = "createFlightBooking")
    @WebResult(name="booking")
    public FlightBooking createFlightBooking(@WebParam(name = "name") @XmlElement(required = true) String name,
                                             @WebParam(name = "flightId") int flightId) throws FlightNotFound, CapacityInsufficient{
        Flight flight = FlightDB.find(flightId);
        if (flight == null){
            throw new FlightNotFound();
        } else if (flight.capacity < 1) {
            throw new CapacityInsufficient();
        }
        flight.capacity -= 1;
        FlightBooking booking = new FlightBooking(name, flightId);
        FlightBookingDB.persist(booking);
        return booking;
    }
    
    @WebMethod(operationName = "removeFlightBooking")
    @WebResult(name = "success")
    public Boolean removeFlightBooking(@WebParam(name = "id") int id) throws FlightBookingNotFound {
        FlightBooking booking = FlightBookingDB.find(id);
        if (booking == null){
            throw new FlightBookingNotFound();
        }
        Flight flight = FlightDB.find(booking.flightId);
        flight.capacity += 1;
        FlightBookingDB.delete(id);
        return true;
    }
}
