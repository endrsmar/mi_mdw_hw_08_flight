/*
 * Created by Martin Endrst as part of MI-MDW course at CTU FIT
 */
package model;

import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author Martin Endrst
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Flight extends BaseEntity {
  
    @XmlElement(required=true)
    public String destination;
    @XmlElement(required=true)
    public String departureAirport;
    @XmlElement(required=true)
    public Date departureDate;
    @XmlElement(required=true)
    public String arrivalAirport;
    @XmlElement(required=true)
    public Date arrivalDate;
    @XmlElement(required=true)
    public int capacity;
    
    public Flight(){}
    public Flight(String destination,
                  String departureAirport,
                  Date departureDate,
                  String arrivalAirport,
                  Date arrivalDate,
                  int capacity)
    {
        this.destination = destination;
        this.departureAirport = departureAirport;
        this.departureDate = departureDate;
        this.arrivalAirport = arrivalAirport;
        this.arrivalDate = arrivalDate;
        this.capacity = capacity;
    }
        public Flight(int id,
                  String destination,
                  String departureAirport,
                  Date departureDate,
                  String arrivalAirport,
                  Date arrivalDate,
                  int capacity)
    {
        this.id = id;
        this.destination = destination;
        this.departureAirport = departureAirport;
        this.departureDate = departureDate;
        this.arrivalAirport = arrivalAirport;
        this.arrivalDate = arrivalDate;
        this.capacity = capacity;
    }
    
}
