/*
 * Created by Martin Endrst as part of MI-MDW class at CTU FIT
 */
package model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author Martin Endrst
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class FlightBooking extends BaseEntity {    
    
    @XmlElement(required=true)
    public String name;
    @XmlElement(required=true)
    public int flightId;
    
    public FlightBooking() { }
    public FlightBooking(String name, int flightId) {
        this.name = name;
        this.flightId = flightId;
    }
    
}
