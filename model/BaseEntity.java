/*
 * Created by Martin Endrst as part of MI-MDW course at CTU FIT
 */
package model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author Martin Endrst
 */
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class BaseEntity implements Serializable{
    
    @XmlElement(required=true)
    public int id;
    
}
