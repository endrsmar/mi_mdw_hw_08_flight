/*
 * Created by Martin Endrst as part of MI-MDW course at CTU FIT
 */
package fault;

/**
 * @author Martin Endrst
 */
public class FlightNotFound extends Exception {
    
    public FlightNotFound(){
        super("FlightNotFound");
    }
    
}
