/*
 * Created by Martin Endrst as part of MI-MDW course at CTU FIT
 */
package fault;

/**
 * @author Martin Endrst
 */
public class FlightBookingNotFound extends Exception {
    
    public FlightBookingNotFound(){
        super("Flight booking not found");
    }
    
}
